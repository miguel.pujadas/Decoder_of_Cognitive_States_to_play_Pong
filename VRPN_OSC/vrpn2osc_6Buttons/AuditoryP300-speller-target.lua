dofile("C:/Archivos de Programa/openvibe/share/openvibe-plugins/stimulation/lua-stimulator-stim-codes.lua")

function shuffle(t)
	local n = #t
	while n >= 2 do
		-- n is now the last pertinent index
		local k = math.random(n) -- 1 <= k <= n
		-- Quick swap
		t[n], t[k] = t[k], t[n]
		n = n - 1
	end
	return t
end

	
function initialize(box)
	number_of_trials = box:get_setting(2)
	number_of_repetitions = box:get_setting(3)
	noteDuration = box:get_setting(4)
	gapBetweenNotes = box:get_setting(5)
	gapBetweenRepetitions = box:get_setting(6)
	note = {OVTK_StimulationId_Label_01, OVTK_StimulationId_Label_02, OVTK_StimulationId_Label_03, OVTK_StimulationId_Label_04, OVTK_StimulationId_Label_05, OVTK_StimulationId_Label_06}
	-- fill the sequence table with predifined order
	
	-- initializes random seed
	math.randomseed(os.time())
	melody = {}
	for i = 1, number_of_trials do
		a = math.random(1, 6)
		table.insert(melody, 1, note[a])
	end
end


function process(box)

	local t=0

	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
--~ 	t = t + 5
--~ 	box:send_stimulation(1, OVTK_StimulationId_BaselineStart, t, 0)
--~ 	t = t + 10
--~ 	box:send_stimulation(1, OVTK_StimulationId_BaselineStop, t, 0)
	
	-- manages trials

	for i = 1, number_of_trials do
		t=t+2
		box:send_stimulation(1, OVTK_StimulationId_TrialStart, t, 0) --stimulation for displaying the target note -- start of subtrial
		box:send_stimulation(1, melody[i], t, 0)
		t = t + noteDuration
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
		t = t + 2
		for j = 1, number_of_repetitions do -- for every trial play 10 random note sequences of all 6 notes
			shuffle(note)
			box:send_stimulation(1,OVTK_GDF_Beep,t,0) --label for the start of a repetition
			for k = 1, 6 do
				box:send_stimulation(1,note[k], t, 0)
				 if note[k] ==melody[i] then 
					box:send_stimulation(1, OVTK_StimulationId_Target, t, 0)
				 else
					box:send_stimulation(1, OVTK_StimulationId_NonTarget, t, 0)
				 end
				t = t + noteDuration
				box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
				t = t + gapBetweenNotes
			end
			t = t + gapBetweenRepetitions
		end
		t = t+2
		--box:send_stimulation(1, OVTK_StimulationId_TrialStop, t, 0) --stimulation for displaying the detected note
		--t = t+1 --time for the result
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0) --result off
		t=t+2
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0) --result off
	end

	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)
	t = t + 5
	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)

end
